function out = BA_simple(dataset1,dataset2,plotname)
% From Peggy's old BA code.
% adapted to be a function
% plotname is either empty or a string.

% prompt1= 'what is the first dataset; enclose in []:';
% dataset1 = input(prompt1);
% prompt2= 'what is the second dataset; enclose in []:';
% dataset2 = input(prompt2);

d1=dataset1(:);
d2=dataset2(:);
n= length(d1);

m=(d1+d2)./2;
d=d1-d2;

diffm=mean(d);
diffs=std(d);
ul=diffm+diffs*1.96;
ll=diffm-diffs*1.96;

% added text to know what the values are
t1 = ['Upper Limit: ' num2str(ul) '; Lower Limit: ' num2str(ll) ': Bias: ' num2str(diffm)];
out = [ ul ll diffm];

plotTitle = 'Bland-Altman plot';
if exist('plotname')
    plotTitle = [plotTitle ': ' plotname ' ' t1];
end
    
figure;
plot(m,d,'o')

% default
% axis([min(m)-0.1*max(m) max(m)+0.1*max(m) diffm-3*diffs diffm+3*diffs])
axis([300 1600 -60 60])  % special for superstan meeting 25 Oct 2019

hold on
plot([min(m)-0.1*max(m),max(m)+0.1*max(m)], [ul, ul],'r-');
plot([min(m)-0.1*max(m),max(m)+0.1*max(m)], [ll, ll],'r-');
plot([min(m)-0.1*max(m),max(m)+0.1*max(m)], [diffm, diffm],'k-','LineWidth',2);
xlabel('mean of datasets')
ylabel('difference of datasets')
title(plotTitle)
x1 = max(m);
y1 = ul;
str1 = '\downarrow +1.96*SD';
text(x1,y1,str1)
x2 = max(m);
y2 = ll;
str2 = '\uparrow -1.96*SD';
text(x2,y2,str2)

display(['Upper Limit: ' num2str(ul) '; Lower Limit: ' num2str(ll) ': Bias: ' num2str(diffm)]);

end

