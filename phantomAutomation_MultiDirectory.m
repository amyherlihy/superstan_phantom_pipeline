% AHH adds another loop to have a group of .mat files run at once
% they must have the same T2 and the same number of ROIs
% You MUST have T1 rois and one set of PDFF rois.

% topFolderName = '/Users/carolinafernandes/Desktop/Test'; % include here directory
outerFolderName = '/Users/amyherlihy/Desktop/RunMe';
D = dir(outerFolderName);
no_of_files = length(D);

for file_no = 1:no_of_files
    mat_file_name = D(file_no).name; %% debugging (put ; back when done)
    MFN = mat_file_name(1:length(mat_file_name)-4);
    file_name = strcat(outerFolderName, '/', mat_file_name);
    matstring = strfind(file_name, '.mat');
    if isdir(file_name)== 0 && isempty(matstring) == 0
        
        
        % topFolderName = '/Users/carolinafernandes/Desktop/Test'; % include here directory
        % topFolderName = '/Users/amyherlihy/Desktop/Automation_Test_Data/RunMe';
        topFolderName = file_name;
        dTop = dir(topFolderName);
        resultsFolder = [outerFolderName '/Results_' MFN];
        
        % resultsFolder = fullfile(topFolderName,'Results');
        
        if ~ exist(resultsFolder,'dir')
            mkdir(resultsFolder);
        end
        
        cd(resultsFolder)
        delete *.*
        dirSize = length(dTop);
        pwd
        
        for idx = 1:dirSize
            
            name = dTop(idx).name;
            fileName = fullfile(outerFolderName,name);
            
            if ~startsWith(name,'.') && ~isfolder(fileName)
                
                lms.loadLmsData(fileName);
                lms_data = getappdata(0,'lms_data');
                app = lms_data.apps.SS_PhantomAnalysisGUI;
                
                numOfImages = length(lms_data.images.t1);
                
                for jdx = 1:numOfImages
                    
                    app.ImagenumberEditField.Value = jdx;
                    app.NumberofROIsEditField.Value = 14; % change number of ROIs
                    app.WaterT2msEditField.Value = 42.5; % include here T2 value in ms Default=18
                    UseLMSPDFFButtonPushedPublic(app);
                    RunButtonPushedPublic(app);
                    ExporttocsvButtonPushedPublic(app,'saveInFolder',resultsFolder);
                    
                end % for
            end % if
            
        end % for
        
    end % If a real file
end  % end number of files
