% Superstan Wraper
% Created 31 October 2019
% Requires all data to be in the "RunMe" directory
% also requires files: DriverFile_One.csv

clear all

% remember what directory you are initially in
% OR you must add your sspipeline dir to your matlab path
matlab_dir = cd; % 

%%
% MoveTo_ALL_RESULTS.mlx
inFolder = '/Users/amyherlihy/Desktop/RunMe/';

% Read in "DriverFile.csv"
DriverFile = readtable([inFolder 'DriverFile_One.csv']);

cd(inFolder)
mkdir('ALL_RESULTS')

for idx = 1:size(DriverFile,1)

        targ = strcat('ALL_RESULTS/', char(DriverFile.ROIStatsFolderName(idx) )   );
        movefile(char(DriverFile.RunMeResults(idx)), targ)    
  
    
end % for idx loop

cd(matlab_dir); % back to matlab direcotry

display('done moving folders to ALL_RESULTS');


%% Create_studyserieslist_files.mlx

% automatically create the studyserieslists
% Takes Input from "ALL_RESULTS" folder & a file much like
% "Description_Processed_xxxxx".csv to be called "DriverFile.csv"
% Keeps the file format established with the FDA3-T1 SS auotmation work.


inputFolder = [ inFolder 'ALL_RESULTS'];

% data on desktop in "make_studyserieslist_tool"

ssl_DirName  = [inFolder 'StudySeriesList/'];
% if file doesn't exist - create it.
if ~exist(ssl_DirName,'dir')
    mkdir(ssl_DirName);
end


% Loop through the "ALL_RESULTS" directory
dFolder = dir(inputFolder);

for idx = 1:length(dFolder)
    
    name = dFolder(idx).name;
    fileName = fullfile(inputFolder,name);
    
    if ~startsWith(name,'.') && isfolder(fileName) % Get a data file
        % Loop through directory getting all data files.
        %name  % debugging
        modelname = name(1:strfind(name,'_')-1);
        
        clear serList
        filecnt = 1;
        rpt = 0;
        SS_files = dir(fileName);
        if contains(name,'SC1')
            rpt = 1;
        end
        if contains(name,'SC2')
            rpt = 2;
        end
        
        for jdx = 1: length(SS_files)
            sFile = SS_files(jdx).name;
            
            % within this folder - get the list of series numbers
            if ~startsWith(sFile,'.') && ~isfolder(sFile) % Get a data file
                % sFile % debugging
                
                serNum = strfind(sFile,'_'); % is this right?
                if size(serNum,2) > 2
                    error % found too many underscores!
                end
                

                s1 = sFile(serNum(1)+1: serNum(2)-1);
                
                
                % handle the case where there is a dash in number and just
                % get first number.  Example 12-14.
                if contains(s1,'-') % case of dash
                    s1 = s1(1: strfind(s1,'-') -1);
                end
                
                g1 = str2num(s1); % get the series number
                
                if filecnt == 1
                    serList = [g1];
                else
                    serList = [serList g1];
                end
                filecnt=filecnt+1;
                % filecnt % debugging
            end % if
            
            
        end % jdx
        
        
        SeriesStart = min(serList);
        SeriesEnd = max(serList);
        
        a1 = strcmp(DriverFile.ScannerModel, modelname);
        a2 = DriverFile.Repeat == rpt;
        x = a1 & a2;
        
        ScanName = DriverFile.ScanName(x);
        Comment = {' '};
        NumVials = DriverFile.NumVials(x);
        PhantomID = DriverFile.Phantom(x);
        FieldStrength = DriverFile.FieldStrength(x);
        Site = DriverFile.Site(x);
        ScannerModel = DriverFile.ScannerModel(x);
        Repeat = DriverFile.Repeat(x);
        
        DriverFile.SeriesStart(x) = SeriesStart;
        DriverFile.SeriesEnd(x) = SeriesEnd;
        
        
        ssl = table(SeriesStart,SeriesEnd,ScanName,Comment,NumVials,PhantomID,FieldStrength,Site,ScannerModel,Repeat);
        
        if rpt == 1
            ssl_d = strcat(ssl_DirName,{'SC1/'});
            ssl_n = strcat(ssl_DirName,{'SC1/StudySeriesList_SC1_'},Site,'_',ScannerModel, '_', string(FieldStrength) , {'T_'} ,PhantomID, {'.csv'});
        end
        if rpt == 2
            ssl_d = strcat(ssl_DirName,{'SC2/'});
            ssl_n = strcat(ssl_DirName,{'SC2/StudySeriesList_SC2_'},Site,'_',ScannerModel, '_', string(FieldStrength) ,{'T_'} ,PhantomID, {'.csv'});
        end
        
        % check if folder exists
        
        % if file doesn't exist - create it.
        if ~exist(string(ssl_d),'dir')
            mkdir(string(ssl_d));
        end
        
        writetable(ssl, ssl_n)
        
        
    end % if  actual-file check
end % for idx loop

% Write out DriverFile with updated info.
writetable(DriverFile,[inFolder 'DriverFile_One.csv']);
% writetable(DriverFile,[ssl_DirName 'DriverFile_One.csv']); % saving it

display(['Finished creating studyserieslists']);


%% Pause to give user a chance to edit study series list for any bad data.
% OR if we can instruct the user to ONLY put ROIs on the data sets that are
% good - basically put all ROIs on and then remove them from unwanted data
% sets - then this removes the need to manually edit the study series list.
% TO BE DETERMINED
%% INPROGRESS

%%  THIS IS THE PARSING SECTION
% Driver for AmyScript_LMSD_PerformanceTestingOutput.
% AHH 9 Oct 2019
% Script Name:  driver_LMSD_PerformanceTestingOutput
% NOTICE THIS CALLS: LMSD_PerformanceTestingOutput.m

cd (matlab_dir)

% The purpose of this is to get the list of input names for the Perfomance
% testing parsing routine so that it can be run in a batch method.

clear all


% Set up all Variables
inFolder = '/Users/amyherlihy/Desktop/RunMe/'; % <<<<< MUST UPDATE THIS
parse_outfile = [inFolder 'Parsed_CSV_Files/'];

if ~exist(parse_outfile)
    mkdir(parse_outfile)
end

info = readtable([inFolder 'DriverFile_One.csv']);
PhantomKeyName = char(info.Phantom(1)); 
PhantomKey = readtable([inFolder PhantomKeyName '.csv']);  %% There is an assumptoin that this is ALL the same phantom! and can use index 1 for all
Num_ROIS = info.NumVials(1); %% %% There is an assumptoin that this is ALL the same phantom! 
top_outfile = [inFolder 'Parser_Output'];

StudySeriesList_Folder = [inFolder 'StudySeriesList'];

statsFolder = [ inFolder 'ALL_RESULTS'];  % location of SScT1 ROIstat files
dStats = dir(statsFolder);


dStats = dir(statsFolder);
filecount = length(dStats);

for idx = 1:filecount
    
    name = dStats(idx).name;
    fileName = fullfile(statsFolder,name);
    
    if ~startsWith(name,'.') && isfolder(fileName) % I'm looking for the results FOLDER
        % Loop through "ALL_RESULTS" directory
        % Parse out the names
        % Get the Series Key file
        % Get the data folder
        a = strfind(name, '_SC');
        scannerName = name(1: a-1);
        site = char( info.Site( strcmp(info.ScannerModel,scannerName) ) );
        s_fld = num2str( info.FieldStrength( strcmp(info.ScannerModel,scannerName) )) ;
        jString = name(a+3);  % Get the 1 or 2 of SC string
         
        outfile = [parse_outfile 'Parsed_' scannerName '_SC' jString]; % save in it's own directory
        SeriesKey = [ StudySeriesList_Folder '/SC' jString '/StudySeriesList_SC' jString '_' site '_' scannerName '_' s_fld 'T_' PhantomKeyName '.csv']; % debugging - missing SITE name
        
        a = LMSD_PerformanceTestingOutput(PhantomKey,SeriesKey,Num_ROIS,outfile,fileName);
        
    end % if  real file check
end % for idx loop

%% Note this puts the Parsed files in the right place. 
display('finised PARSING Step - Output is LMS-MD Performance Testing output style');



%%  Create files that will be used by Arina in Biostats for her analysis
% Create_FlaggedComparisons_file.mlx
% Create FlaggedComparisons file
% This will build a vanilla FlaggedComparisons file.
% It populates the SC1 and SC2 series with the first series run for each of
% the repeats.
% If you have data where you don't want to use the first MOLLI scan - you
% will need to manually edit this file and add the comments and extra flags
% for Biostats to use in their pipeline.

clear all

inFolder = '/Users/amyherlihy/Desktop/RunMe/';
R_folder = [inFolder 'Parsed_CSV_Files'];
driver = readtable([inFolder 'DriverFile_One.csv']);
phantName = driver.Phantom(1); % assumption it is all the same phantom.


dFolder = dir(R_folder);
filecount = length(dFolder);
filecnt = 1;

for idx = 1:filecount
    
    name = dFolder(idx).name;
    fileName = fullfile(R_folder,name);
    
    if ~startsWith(name,'.') && ~isfolder(fileName) % Get a data folder
        % Loop through directory getting all data files.
        % name  % debugging
        pName{filecnt} = name;
        t = strfind(name,'_');
        model = name(t(1)+1:t(2)-1);
        
        aa = strcmp(driver.ScannerModel,model);
        bb = (driver.Repeat == 1);
       
        sToUse(filecnt) = driver.SeriesStart(aa & bb );
        sEmpty{filecnt} = '';
        sn{filecnt} = 'n';

        filecnt=filecnt+1; 
    end % if  real file check
end % for idx loop
% 

varNames = {'DataSet', 'SeriesToUse', 'SeriesNotFirst', 'WhyNotFirstSeries', 'IgnoreVials', 'Notes' };
pTable = table(pName', sToUse', sn', sEmpty', sEmpty', sEmpty', 'VariableNames',varNames);

outname = [char(inFolder) 'FlaggedComparisons_' char(phantName) '_Data_Files.csv'];
writetable(pTable, outname);


display('Finished making FlaggedComparions file.')





% Create_Description_Processed_file.mlx
% Create Description_Process_file
% This takes the DriverFile_One.csv  and makes
% the Description_Processed_FDA3-T1_SS_TEST_Data_Files.csv file
% Assumption that Arina will want the "Description_Processed" file.
% this also updates the DriverFile_One with the ParseFilename info. 
% Must be run AFTER the Parsing is done.

clear all

inFolder = '/Users/amyherlihy/Desktop/RunMe/';
driver = readtable([inFolder 'DriverFile_One.csv']);
phantName = driver.Phantom(1); % assumption it is all the same phantom.

varNames = {'Filename','Phantom','Site','ScannerModel','ScannerManufacturer','FieldStrength','Repeat' };

% get the parsed data names - match with scanner model

R_folder = [inFolder 'Parsed_CSV_Files'];
dFolder = dir(R_folder);
filecount = length(dFolder);
filecnt = 1;

for idx = 1:filecount
    
    name = dFolder(idx).name;
    fileName = fullfile(R_folder,name);
    
    if ~startsWith(name,'.') && ~isfolder(fileName) % Get a data folder
        % Loop through directory getting all data files.
        % name  % debugging

        t = strfind(name,'_');
        model = name(t(1)+1:t(2)-1);
        
        aa = strcmp(driver.ScannerModel,model);
       
        driver.ParseFilename(aa) = cellstr(name);

        filecnt=filecnt+1; 
    end % if  real file check
end % for idx loop

descrip_data = table(driver.ParseFilename, driver.Phantom, driver.Site, driver.ScannerModel, driver.ScannerManufacturer, driver.FieldStrength, driver.Repeat,'VariableNames',varNames );

% Example Name:  Description_Processed_FDA3-T1_SS_TEST_Data_Files.csv
outname = [char(inFolder) 'Description_Processed_' char(phantName) '_Data_Files.csv'];
writetable(descrip_data, outname);
% update DriverFile_One
writetable(driver,[inFolder 'DriverFile_One.csv']);


display('Finished making Description_Processed file.')




