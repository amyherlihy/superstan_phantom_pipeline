function [done] = AmyScript_LMSD_PerformanceTestingOutput(PhantomKey,SeriesKey_file,Num_ROIS,outfile,data_folder_name)
%% Called:   AmyScript_LMSD_PerformanceTestingOutput.m
%% NOTICE  This does NOT handle the original Superstan csv format.
% It only handles SS data of format similar to the "ROIStats" 7 Oct 2019
% The output will be in MEDICAL DEVICE format
% Note - that many fields are populated with fake data. These fields are
% unused in the stats analysis. These fields are not available in the LMSD
% output - but are in the medical device output and this is why fake data
% is used.

% Copied from AmyScript_ROIStats_VialNum_Field (on 3 Oct 2019)

% How this works:
%    Read In the StudySeries List
%    Read In the Phantom Key
%    Read In the output file (the output from the VialNum_Field
%    <move things around>
%    OUTPUT will be in the format of the PARSED medical device data
%           ready for Innovation to work on.
%
% Note: SS output code is contained in here:   SS_PhantomAnalysisGUI.mlapp


%% Some definitions
%% Change File Names
% % PhantomKey = readtable('/Users/amyherlihy/Desktop/PD_AH_QC1_16vial.csv');  %% (this has the right vial loactions)
% % SeriesKey =  readtable('/Users/amyherlihy/Desktop/StudySeriesList_12April2017_15T_UKBB-Newcastle_ScanSet2-OldQC1.csv');
% % Num_ROIS = 16; %% This needs updating to match the number of vials!
% % outfile = 'OUTPUT_12April2017_15T_UKBB-Newcastle_ScanSet2-OldQC1';
% % fieldstrength = 15;  %% either 15 or 3

% PhantomKey = readtable(PhantomKey_file);  %% This is a special FDA3-T1 version
SeriesKey =  readtable(SeriesKey_file);
% Num_ROIS = 14; %% This needs updating to match the number of vials!
% outfile = '/Users/amyherlihy/Desktop/Biograph_sc1';
med_outfile = [outfile '_MED'];
med_csv_outfile = [outfile '_csvMED.csv'];
% SS work. it handles the SScT1 and SST1 roi stats.
SuperStanFlag = 3;  % set to 3 for SS work or set to 0 for not SS work.
done = 0; %debugging



%% strings must all be of length 4 for the table/csv stuff. define it here.
T1string = 't1  ';
PDFFstring = 'pdff';
T2starstring='t2s ';
SScT1string='scT1';
SST1string='ssT1';

D = dir(data_folder_name);
no_of_files = length(D);

% Phantom key info - stored in one place.
% get phantom info into tables  (for now on desktop)
%%  PhantomKey = readtable('Testworkbook.csv'); %% Test Code
%%  SeriesKey = readtable('StudySeriesList_30Sept2016_1_5T.xlsx');  %% Test Code
%% See directory LizCode   ROI_Test_DIr - look at files there. It should all work. Must specify field strength
%% Notice the ROIStats.csv file names must begin with pdff/t2s/t1 - not with the study name.

filecnt=1; % count of files read in.
%% Read in one file from D
for file_no = 1:no_of_files

    just_file_name = D(file_no).name; %% debugging (put ; back when done)
    file_name = strcat(data_folder_name, '/', just_file_name);
    csvstring = strfind(file_name, '.csv');
    if isdir(file_name)== 0 && isempty(csvstring) == 0
        b = just_file_name(1:4);
        lengthfilename=length(just_file_name);
        AnalysisType='error';
        
        
        %% For magnitude data - No range of pdff or T1 so no dash present. For Complex data -  pdff and T1 have 2 series, so a dash exists.
        if ~isempty(strfind(b,'t1'))   % AND NOT scT1 and NOT ssT1
            AnalysisType=T1string;
            c=just_file_name(4:lengthfilename);
            % >>>>> o More checks here for ssT1 and scT1
            if strfind(c,'-')  SeriesNum = str2num(c(1:strfind(c,'-')-1));
            else SeriesNum = str2num(c(1:strfind(c,'_')-1));
            end
        elseif ~isempty(strfind(b,'t2s'))
            AnalysisType=T2starstring;
            c=just_file_name(5:lengthfilename);
            %% SeriesNum = str2num(c(1:strfind(c,'-')-1)     );
            if strfind(c,'-')  SeriesNum = str2num(c(1:strfind(c,'-')-1));
            else SeriesNum = str2num(c(1:strfind(c,'_')-1));
            end
        elseif ~isempty(strfind(b,'pdff'))
            AnalysisType=PDFFstring;
            c=just_file_name(6:lengthfilename);
            %% SeriesNum = str2num(c(1:strfind(c,'-')-1)     );
            if strfind(c,'-')  SeriesNum = str2num(c(1:strfind(c,'-')-1));
            else SeriesNum = str2num(c(1:strfind(c,'_')-1));
            end
        elseif ~isempty(strfind(b,'SScT'))  % only looking for 4 chars
            AnalysisType=SScT1string;
            c=just_file_name(7:lengthfilename); % different length base name.
            %% SeriesNum = str2num(c(1:strfind(c,'-')-1)     );
            if strfind(c,'-')  SeriesNum = str2num(c(1:strfind(c,'-')-1));
            else SeriesNum = str2num(c(1:strfind(c,'_')-1));
            end
        elseif ~isempty(strfind(b,'SST1'))
            AnalysisType=SST1string;
            c=just_file_name(6:lengthfilename);
            %% SeriesNum = str2num(c(1:strfind(c,'-')-1)     );
            if strfind(c,'-')  SeriesNum = str2num(c(1:strfind(c,'-')-1));
            else SeriesNum = str2num(c(1:strfind(c,'_')-1));
            end
        end
        
        ROIData=readtable(file_name);
        %%        [mean,stdev,stdev_mean,diam,median,min,max,x,y,index]=csvimport(file_name,'columns',{'mean','stdev','stdev_mean','diam','median','min','max','x','y','index'});
        %% Important to have NO extra study name in file names NOTE THIS
        %% Get Flip Angle from SeriesKey
        x=size(SeriesKey);
        count=1; %counter to work through the series list.
        while count <= x(1)
            if ((SeriesKey.SeriesStart(count) <= SeriesNum) && (SeriesKey.SeriesEnd(count) >= SeriesNum))
                scanName = SeriesKey.ScanName(count);
                dd = SeriesKey.NumVials(count);
                fieldstrength = SeriesKey.FieldStrength(count);
                count=x(1)+1;  % at the end to exit out of loop
            else  count=count+1;
            end
            
            
        end
        
        % Get site information from Series Key
count = 1;  %AHH - For this exercise - the Study Series Keys will only have one line.  
% perhaps fix this later AHH
ss = SeriesKey.ScanName(count);
NumberOfVials = SeriesKey.NumVials(count);
PhantomName = SeriesKey.PhantomID{count};
if size(SeriesKey,2) == 10 %% For FDA work
    a_Repeat = SeriesKey.Repeat(count);
    a_Site = SeriesKey.Site(count);
    a_ScannerModel = SeriesKey.ScannerModel(count);
    
    VialType = PhantomKey.VialType;
    if fieldstrength == 3;
        ExpectedValue = PhantomKey.Expected_3TSiem;
        if size(PhantomKey,2) == 27; %% special Phantom Key
            AcceptedValue = PhantomKey.MOLLI_Accepted3T;
            LowerAccept = PhantomKey.MOLLI_Lower3T;
            UpperAccept = PhantomKey.MOLLI_Upper3T;
        end
    else
        ExpectedValue = PhantomKey.Expected_1_5TSiem;
        if size(PhantomKey,2) == 27; %% special Phantom Key
            AcceptedValue = PhantomKey.MOLLI_Accepted1_5T;
            LowerAccept = PhantomKey.MOLLI_Lower1_5T;
            UpperAccept = PhantomKey.MOLLI_Upper1_5T;
        end
    end
% 
else
    burp
    % The SeriesKey file is a size we don't recognize. Expect 10 columns. 
end


        
        %% - error catching - if scanName and or dd are empty - then it couldn't find the right series! - stop and say files of info don't match.
        %% ALSO - never have extra - or _ in the name. messes things up
        %% Notice FlipAngle no longer of interest. ONly useful info in FA is the 19431 flag that indicates IDEAL
        FlipAngle=999; %% if it is not T2* or PDFF then set FA to 999.
        if ((AnalysisType ==PDFFstring) | (AnalysisType ==T2starstring))
            FlipAngle=20;
            a=string(scanName);
            if ~isempty(strfind(a,'IDEAL'))
                FlipAngle=19341;   %% Added the IDEAL analysis type
            end
            clear a;
            %             if ~isempty(strfind(scanName,'FLIP 6')) FlipAngle=6; %% BUG - scanName not defined - so never got into the loop above.
            %             end
        end
        
        % New special code for the SST1 and SScT1 files. If these are found they get put in a special place to add to the T1 data - which it DOES expect.
        % =============================
        if SuperStanFlag == 3 % so this can be used if needed.
            % Get this data and put it in the SuperStan structure to wait
            % to get paired with the T1 file.
            if (AnalysisType == T1string)
                    zz = zeros( size(ROIData,2),1 );
                    rSquared = zz;
                    PDFF = zz;
                % This is the T1 bit and need to get it all combined.
                % T1 file should appear after the ss files. Assumption.
                % Rsquared & PDFF is the same for both SST1 and SScT1
                
                SSData = table(rSquared, PDFF);
                
                % Combine to a new table.
                TempData= [ROIData SSData];
                clear ROIData;
                ROIData = TempData;
            end
            
        end
        % End special super stan code (to add 2 columns to the T1 data) 
        % =============================
        
        %
        % Use PhantomKey
        % Get ExpectedValue
        % Get VialType
        r=ROIData.index;
        VialType = PhantomKey.VialType(r);  % (Consider having vial type = SS ... after making R&D checks)
        %% LOOK HERE for field strength
        % Newer phantom keys don't have expected 3T - hope Arina doesn't
        % want it. 
        if (fieldstrength == 1.5)
            Expected3TSiemens = PhantomKey.Expected_1_5TSiem(r);
        else
            if (fieldstrength == 3)
                Expected3TSiemens = PhantomKey.Expected_3TSiem(r);
            else
                Expected3TSiemens = 'blupb'; %Purposeful bug  you got the field strength wrong.
            end
        end
        
        cc = repmat(AnalysisType,Num_ROIS,1);
        AnalysisType  = cellstr(cc);
        SeriesNum = repmat(SeriesNum,Num_ROIS,1);
        FlipAngle = repmat(FlipAngle,Num_ROIS,1);
        PhantomName	= PhantomKey.PhantomSN2;
        NewTable = table(AnalysisType, SeriesNum, FlipAngle, VialType, AcceptedValue,LowerAccept, UpperAccept ,Expected3TSiemens,PhantomName);
        X = [ROIData NewTable];
        

        %% Add data to existing table
        if (filecnt == 1)
            W_table= X;
        else
            W_table= vertcat(W_table,X);
        end
        filecnt=filecnt+1;
        
    end % End of activity for a good file
    
end

% writetable(W_table,outfile)


%% ==============================
% PhantomKey & Series Key things

%     if size(PhantomKey,2) == 8; %% Older Phantom Key
%         B(line_cnt,:) = [VialLocation, VialType, ExpectedValue, PhantomName,field];
%     elseif size(PhantomKey,2) == 14; %% New Phantom Key
%         B(line_cnt,:) = [VialLocation, VialType, ExpectedValue, PhantomName,field, AcceptedValue, LowerAccept, UpperAccept ];
%     else
%         burp %% intentional error. Debug the code.
%     end
%     if size(SeriesKey,2) == 10 %% For FDA work
%         C(line_cnt,:) = [Site, ScannerModel, Repeat];
%     end
%     
 %% =============================

%% Now adjust it for the Medical Device style output.
% fill in unavailable category numbers with zeros  or a string "NA"
c = size( W_table,1);
zz = zeros(c,1);
a = 'NA';

for i=1:c
    na{i} = a;
    rtype{i} = 'cT1';
    SC{i} = a_Repeat;
    Location{i} = a_Site;
    Scanner{i} = a_ScannerModel;
    Field{i} = fieldstrength;
end

 %% =============================
 %% NOTICE _ this assumse that all data is SScT1 (for work Oct 2019).
 % data only goes in T1Mean and T1median

ROIType = rtype';  %special code to get the cT1 for Arina's analysis
ROINum	= (1:length(ROIType))';  % just a count of all ROIs (like in MedDev)
Diam	 = W_table.diameter;
CenterX	= W_table.x;
CenterY	= W_table.y;
SeriesNum	= W_table.SeriesNum;
Median	= zz;
LowerQuart	= zz;
UpperQuart	= zz;
Mean	   = zz;
R2Median	= zz;
R2LowerQuart	= zz;
R2UpperQuart= zz;	
R2Mean	= zz;
T1Median = W_table.median; % SScT1
T1LowerQuart	= zz;
T1UpperQuart	= zz;
T1Mean	= W_table.mean; % SScT1
GroupSize	= zz;
GroupID	= zz;
VialLocation = W_table.index;
VialType	= W_table.VialType;
ExpectedValue	= W_table.Expected3TSiemens;
PhantomName = W_table.PhantomName;
AcceptedValue = W_table.AcceptedValue;
LowerAccept = W_table.LowerAccept;
UpperAccept = W_table.UpperAccept;
Site = Location';
ScannerModel = Scanner';
Repeat = SC';
FieldStrength = Field';
    

% debugging check
% 
% size(ROIType')
% size(ROINum)
% size(Diam)
% size(CenterX)
% size(CenterY)
% size(SeriesNum)
% size(Median)
% size(LowerQuart)
% size(UpperQuart)
% size(Mean)
% size(R2Median)
% size(R2LowerQuart)
% size(R2UpperQuart)
% size(R2Mean)
% size(T1Median)
% size(T1LowerQuart)
% size(T1UpperQuart)
% size(T1Mean)
% size(GroupSize)
% size(GroupID)
% size(VialLocation)
% size(VialType)
% size(ExpectedValue)
% size(PhantomName)
% size(FieldStrength')
% size(AcceptedValue)
% size(LowerAccept)
% size(UpperAccept)
% size(Site')
% size(ScannerModel')
% size(Repeat')

MedDev_table = table(ROIType,ROINum,Diam,CenterX,CenterY, SeriesNum, Median, LowerQuart, UpperQuart, Mean, R2Median, R2LowerQuart, R2UpperQuart, R2Mean, T1Median, T1LowerQuart, T1UpperQuart, T1Mean, GroupSize, GroupID, VialLocation, VialType, ExpectedValue, PhantomName, FieldStrength, AcceptedValue, LowerAccept, UpperAccept, Site, ScannerModel, Repeat);

% writetable(MedDev_table,med_outfile); % txt output - not really useful.

writetable(MedDev_table,med_csv_outfile); % csv output

done = 1; % debugging
% and - I found the data in the "myData.csv" file too.




    
    
    
    
