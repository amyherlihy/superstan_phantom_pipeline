% Driver for AmyScript_LMSD_PerformanceTestingOutput.
% AHH 9 Oct 2019
% Script Name:  driver_LMSD_PerformanceTestingOutput

% The purpose of this is to get the list of input names for the Perfomance
% testing parsing routine so that it can be run in a batch method.

clear all


% Set up all Variables
topdir = '/Users/amyherlihy/Desktop/RunMe/'; % <<<<< MUST UPDATE THIS
parse_outfile = [topdir 'Parsed_CSV_Files/'];

if ~exist(parse_outfile)
    mkdir(parse_outfile)
end

info = readtable([topdir 'DriverFile_One.csv']);
PhantomKeyName = char(info.Phantom(1)); 
PhantomKey = readtable([topdir PhantomKeyName '.csv']);  %% There is an assumptoin that this is ALL the same phantom! and can use index 1 for all
Num_ROIS = info.NumVials(1); %% %% There is an assumptoin that this is ALL the same phantom! 
top_outfile = [topdir 'Parser_Output'];

StudySeriesList_Folder = [topdir 'StudySeriesList'];

statsFolder = [ topdir 'ALL_RESULTS'];  % location of SScT1 ROIstat files
dStats = dir(statsFolder);


dStats = dir(statsFolder);
filecount = length(dStats);

for idx = 1:filecount
    
    name = dStats(idx).name;
    fileName = fullfile(statsFolder,name);
    
    if ~startsWith(name,'.') && isfolder(fileName) % I'm looking for the results FOLDER
        % Loop through "ALL_RESULTS" directory
        % Parse out the names
        % Get the Series Key file
        % Get the data folder
        a = strfind(name, '_SC');
        scannerName = name(1: a-1);
        site = char( info.Site( strcmp(info.ScannerModel,scannerName) ) );
        s_fld = num2str( info.FieldStrength( strcmp(info.ScannerModel,scannerName) )) ;
        jString = name(a+3);  % Get the 1 or 2 of SC string
         
        outfile = [parse_outfile 'Parsed_' scannerName '_SC' jString]; % save in it's own directory
        SeriesKey = [ StudySeriesList_Folder '/SC' jString '/StudySeriesList_SC' jString '_' site '_' scannerName '_' s_fld 'T_' PhantomKeyName '.csv']; % debugging - missing SITE name
        
        a = LMSD_PerformanceTestingOutput(PhantomKey,SeriesKey,Num_ROIS,outfile,fileName);
        
    end % if  real file check
end % for idx loop

display('finised PARSING Step - Output is LMS-MD Performance Testing output style');